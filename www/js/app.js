// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('epat', ['ionic', 'starter.controllers', "highcharts-ng", 'lbServices', 'http-auth-interceptor'])

    .run(function ($ionicPlatform, $location, $rootScope) {

        $rootScope.baseUrl = 'http://epat.zigron.com/api/';

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            $rootScope.isAuth = false;

            $rootScope.$on('event:auth-loginRequired', function () {
                $rootScope.isAuth = false;
                $location.path('/login');
                console.log('Not Authorized!');
            });

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                console.log('stateChanged');

                if (toState.name != 'login' && $rootScope.isAuth == false) {
                    $location.path('/login');
                }
                else if (toState.name == 'login' && $rootScope.isAuth == true) {
                    $location.path('/dashboard');
                }
            });

            var checkLogin = function () {

                if (localStorage.$LoopBack$accessTokenId) {
                    $rootScope.isAuth = true;
                }
                else
                    $location.path('/login');
            };

            checkLogin();

        });

    })



    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $ionicConfigProvider.views.maxCache(0);
        $stateProvider
            .state('ITUEStateName', {
                url: '/ITUE',
                templateUrl: 'templates/instructions.html',
                controller: 'ITUECtrl'
            })
            .state('ASIStateName', {
                url: '/ASI',
                templateUrl: 'templates/accountSetting.html',
                controller: 'ASICtrl'
            })
            .state('MNStateName', {
                url: '/MN',
                templateUrl: 'templates/manageNotifications.html',
                controller: 'MNCtrl'
            })
            .state('BSIStateName', {
                url: '/BSI',
                templateUrl: 'templates/billScriptsInfo.html',
                controller: 'BSICtrl'
            })
            .state('app', {
                url: '/epat',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.Notifications', {
                url: '/Notifications',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/notifications.html',
                        controller: 'notificationsCtrl'
                    }
                }
            })

            .state('app.billcurrent', {
                url: '/bill-current',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/bill-current.html',
                        controller: 'billCtrl'
                    }
                }
            })

            .state('app.billprojected', {
                url: '/bill-projected',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/bill-projected.html',
                        controller: 'billCtrl'
                    }
                }
            })

            .state('app.billpast', {
                url: '/bill-past',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/bill-past.html',
                        controller: 'billCtrl'
                    }
                }
            })





            .state('app.setting', {
                url: '/setting',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/setting.html',
                        controller: "settingCtrl"
                    }
                }
            })
            .state('app.help', {
                url: '/help',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/help.html',
                        controller: "helpCtrl"
                    }
                }
            })
            .state('app.dashboard', {
                url: '/dashboard',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/dashboard-new.html',
                        controller: "DashboardCtrl"
                    }
                }
            })
            .state('app.logout', {
                url: '/logout',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/logout.html',
                        controller: "LogoutCtrl"
                    }
                }
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'loginCtrl'
            })


            .state('registration', {
                url: '/registration',

                templateUrl: 'templates/registration.html',
                controller: 'registrationCtrl'


            })


            .state('forgotPassword', {
                url: '/forgotPassword',
                templateUrl: 'templates/forgotPassword.html',
                controller: "forgotpasswordCtrl"
            })

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/epat/dashboard');
    });
