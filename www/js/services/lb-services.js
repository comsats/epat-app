(function(window, angular, undefined) {'use strict';

  var urlBase = "http://epat.zigron.com/api";
  var authHeader = 'authorization';

  /**
   * @ngdoc overview
   * @name lbServices
   * @module
   * @description
   *
   * The `lbServices` module provides services for interacting with
   * the models exposed by the LoopBack server via the REST API.
   *
   */
  var module = angular.module("lbServices", ['ngResource']);

  /**
   * @ngdoc object
   * @name lbServices.Email
   * @header lbServices.Email
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `Email` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "Email",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/Emails/:id",
        { 'id': '@id' },
        {
        }
      );




      /**
       * @ngdoc property
       * @name lbServices.Email#modelName
       * @propertyOf lbServices.Email
       * @description
       * The name of the model represented by this $resource,
       * i.e. `Email`.
       */
      R.modelName = "Email";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.Coffeeshop
   * @header lbServices.Coffeeshop
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `Coffeeshop` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "Coffeeshop",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/Coffeeshops/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#create
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Coffeeshop` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/Coffeeshops",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#createMany
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Coffeeshop` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/Coffeeshops",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#upsert
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Coffeeshop` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/Coffeeshops",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#exists
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/Coffeeshops/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#findById
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Coffeeshop` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/Coffeeshops/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#find
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Coffeeshop` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/Coffeeshops",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#findOne
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Coffeeshop` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/Coffeeshops/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#updateAll
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/Coffeeshops/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#deleteById
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Coffeeshop` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/Coffeeshops/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#count
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/Coffeeshops/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#prototype$updateAttributes
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - PersistedModel id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Coffeeshop` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/Coffeeshops/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.Coffeeshop#createChangeStream
           * @methodOf lbServices.Coffeeshop
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/Coffeeshops/change-stream",
            method: "POST"
          },
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.Coffeeshop#updateOrCreate
       * @methodOf lbServices.Coffeeshop
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `Coffeeshop` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.Coffeeshop#update
       * @methodOf lbServices.Coffeeshop
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.Coffeeshop#destroyById
       * @methodOf lbServices.Coffeeshop
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `Coffeeshop` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.Coffeeshop#removeById
       * @methodOf lbServices.Coffeeshop
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `Coffeeshop` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];


      /**
       * @ngdoc property
       * @name lbServices.Coffeeshop#modelName
       * @propertyOf lbServices.Coffeeshop
       * @description
       * The name of the model represented by this $resource,
       * i.e. `Coffeeshop`.
       */
      R.modelName = "Coffeeshop";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.UserEpat
   * @header lbServices.UserEpat
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `UserEpat` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "UserEpat",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/UserEpats/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#prototype$__findById__accessTokens
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Find a related item by id for accessTokens.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - User id
           *
           *  - `fk` – `{*}` - Foreign key for accessTokens
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "prototype$__findById__accessTokens": {
            params: {
              'fk': '@fk'
            },
            url: urlBase + "/UserEpats/:id/accessTokens/:fk",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#prototype$__destroyById__accessTokens
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Delete a related item by id for accessTokens.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - User id
           *
           *  - `fk` – `{*}` - Foreign key for accessTokens
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * This method returns no data.
           */
          "prototype$__destroyById__accessTokens": {
            params: {
              'fk': '@fk'
            },
            url: urlBase + "/UserEpats/:id/accessTokens/:fk",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#prototype$__updateById__accessTokens
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Update a related item by id for accessTokens.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - User id
           *
           *  - `fk` – `{*}` - Foreign key for accessTokens
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "prototype$__updateById__accessTokens": {
            params: {
              'fk': '@fk'
            },
            url: urlBase + "/UserEpats/:id/accessTokens/:fk",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#prototype$__get__accessTokens
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Queries accessTokens of UserEpat.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - User id
           *
           *  - `filter` – `{object=}` -
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "prototype$__get__accessTokens": {
            isArray: true,
            url: urlBase + "/UserEpats/:id/accessTokens",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#prototype$__create__accessTokens
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Creates a new instance in accessTokens of this model.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - User id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "prototype$__create__accessTokens": {
            url: urlBase + "/UserEpats/:id/accessTokens",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#prototype$__delete__accessTokens
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Deletes all accessTokens of this model.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - User id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * This method returns no data.
           */
          "prototype$__delete__accessTokens": {
            url: urlBase + "/UserEpats/:id/accessTokens",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#prototype$__count__accessTokens
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Counts accessTokens of UserEpat.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - User id
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "prototype$__count__accessTokens": {
            url: urlBase + "/UserEpats/:id/accessTokens/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#create
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/UserEpats",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#createMany
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/UserEpats",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#upsert
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/UserEpats",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#exists
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/UserEpats/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#findById
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/UserEpats/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#find
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/UserEpats",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#findOne
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/UserEpats/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#updateAll
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/UserEpats/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#deleteById
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/UserEpats/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#count
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/UserEpats/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#prototype$updateAttributes
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - User id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/UserEpats/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#createChangeStream
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/UserEpats/change-stream",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#login
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Login a user with username/email and password.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
           *   Default value: `user`.
           *
           *  - `rememberMe` - `boolean` - Whether the authentication credentials
           *     should be remembered in localStorage across app/browser restarts.
           *     Default: `true`.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The response body contains properties of the AccessToken created on login.
           * Depending on the value of `include` parameter, the body may contain additional properties:
           *
           *   - `user` - `{User}` - Data of the currently logged in user. (`include=user`)
           *
           *
           */
          "login": {
            params: {
              include: "user"
            },
            interceptor: {
              response: function(response) {
                var accessToken = response.data;
                LoopBackAuth.setUser(accessToken.id, accessToken.userId, accessToken.user);
                LoopBackAuth.rememberMe = response.config.params.rememberMe !== false;
                LoopBackAuth.save();
                return response.resource;
              }
            },
            url: urlBase + "/UserEpats/login",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#logout
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Logout a user with access token.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `access_token` – `{string}` - Do not supply this argument, it is automatically extracted from request headers.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * This method returns no data.
           */
          "logout": {
            interceptor: {
              response: function(response) {
                LoopBackAuth.clearUser();
                LoopBackAuth.clearStorage();
                return response.resource;
              }
            },
            url: urlBase + "/UserEpats/logout",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#confirm
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Confirm a user registration with email verification token.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `uid` – `{string}` -
           *
           *  - `token` – `{string}` -
           *
           *  - `redirect` – `{string=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * This method returns no data.
           */
          "confirm": {
            url: urlBase + "/UserEpats/confirm",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#resetPassword
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Reset password for a user with email.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * This method returns no data.
           */
          "resetPassword": {
            url: urlBase + "/UserEpats/reset",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#bill
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * <em>
           * (The remote method definition does not provide any description.)
           * </em>
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{number=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "bill": {
            url: urlBase + "/UserEpats/bill",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#projectedBill
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * <em>
           * (The remote method definition does not provide any description.)
           * </em>
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{number=}` -
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `UserEpat` object.)
           * </em>
           */
          "projectedBill": {
            isArray: true,
            url: urlBase + "/UserEpats/projectedBill",
            method: "GET"
          },

          // INTERNAL. Use PowerUsage.user() instead.
          "::get::PowerUsage::user": {
            url: urlBase + "/PowerUsages/:id/user",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.UserEpat#getCurrent
           * @methodOf lbServices.UserEpat
           *
           * @description
           *
           * Get data of the currently logged user. Fail with HTTP result 401
           * when there is no user logged in.
           *
           * @param {function(Object,Object)=} successCb
           *    Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *    `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           */
          "getCurrent": {
            url: urlBase + "/UserEpats" + "/:id",
            method: "GET",
            params: {
              id: function() {
                var id = LoopBackAuth.currentUserId;
                if (id == null) id = '__anonymous__';
                return id;
              },
            },
            interceptor: {
              response: function(response) {
                LoopBackAuth.currentUserData = response.data;
                return response.resource;
              }
            },
            __isGetCurrentUser__ : true
          }
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.UserEpat#updateOrCreate
       * @methodOf lbServices.UserEpat
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `UserEpat` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.UserEpat#update
       * @methodOf lbServices.UserEpat
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.UserEpat#destroyById
       * @methodOf lbServices.UserEpat
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `UserEpat` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.UserEpat#removeById
       * @methodOf lbServices.UserEpat
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `UserEpat` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.UserEpat#getCachedCurrent
       * @methodOf lbServices.UserEpat
       *
       * @description
       *
       * Get data of the currently logged user that was returned by the last
       * call to {@link lbServices.UserEpat#login} or
       * {@link lbServices.UserEpat#getCurrent}. Return null when there
       * is no user logged in or the data of the current user were not fetched
       * yet.
       *
       * @returns {Object} A UserEpat instance.
       */
      R.getCachedCurrent = function() {
        var data = LoopBackAuth.currentUserData;
        return data ? new R(data) : null;
      };

      /**
       * @ngdoc method
       * @name lbServices.UserEpat#isAuthenticated
       * @methodOf lbServices.UserEpat
       *
       * @returns {boolean} True if the current user is authenticated (logged in).
       */
      R.isAuthenticated = function() {
        return this.getCurrentId() != null;
      };

      /**
       * @ngdoc method
       * @name lbServices.UserEpat#getCurrentId
       * @methodOf lbServices.UserEpat
       *
       * @returns {Object} Id of the currently logged-in user or null.
       */
      R.getCurrentId = function() {
        return LoopBackAuth.currentUserId;
      };

      /**
       * @ngdoc property
       * @name lbServices.UserEpat#modelName
       * @propertyOf lbServices.UserEpat
       * @description
       * The name of the model represented by this $resource,
       * i.e. `UserEpat`.
       */
      R.modelName = "UserEpat";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.PowerUsage
   * @header lbServices.PowerUsage
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `PowerUsage` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "PowerUsage",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/PowerUsages/:id",
        { 'id': '@id' },
        {

          // INTERNAL. Use PowerUsage.user() instead.
          "prototype$__get__user": {
            url: urlBase + "/PowerUsages/:id/user",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#create
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `PowerUsage` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/PowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#createMany
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `PowerUsage` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/PowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#upsert
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `PowerUsage` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/PowerUsages",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#exists
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/PowerUsages/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#findById
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `PowerUsage` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/PowerUsages/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#find
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `PowerUsage` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/PowerUsages",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#findOne
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `PowerUsage` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/PowerUsages/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#updateAll
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/PowerUsages/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#deleteById
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `PowerUsage` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/PowerUsages/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#count
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/PowerUsages/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#prototype$updateAttributes
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - PersistedModel id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `PowerUsage` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/PowerUsages/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.PowerUsage#createChangeStream
           * @methodOf lbServices.PowerUsage
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/PowerUsages/change-stream",
            method: "POST"
          },
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.PowerUsage#updateOrCreate
       * @methodOf lbServices.PowerUsage
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `PowerUsage` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.PowerUsage#update
       * @methodOf lbServices.PowerUsage
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.PowerUsage#destroyById
       * @methodOf lbServices.PowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `PowerUsage` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.PowerUsage#removeById
       * @methodOf lbServices.PowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `PowerUsage` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];


      /**
       * @ngdoc property
       * @name lbServices.PowerUsage#modelName
       * @propertyOf lbServices.PowerUsage
       * @description
       * The name of the model represented by this $resource,
       * i.e. `PowerUsage`.
       */
      R.modelName = "PowerUsage";


      /**
       * @ngdoc method
       * @name lbServices.PowerUsage#user
       * @methodOf lbServices.PowerUsage
       *
       * @description
       *
       * Fetches belongsTo relation user.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - PersistedModel id
       *
       *  - `refresh` – `{boolean=}` -
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `UserEpat` object.)
       * </em>
       */
      R.user = function() {
        var TargetResource = $injector.get("UserEpat");
        var action = TargetResource["::get::PowerUsage::user"];
        return action.apply(R, arguments);
      };

      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.Bill
   * @header lbServices.Bill
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `Bill` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "Bill",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/Bills/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.Bill#getBill
           * @methodOf lbServices.Bill
           *
           * @description
           *
           * <em>
           * (The remote method definition does not provide any description.)
           * </em>
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `referenceNo` – `{string=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Bill` object.)
           * </em>
           */
          "getBill": {
            url: urlBase + "/Bills/getBill",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.Bill#invoke
           * @methodOf lbServices.Bill
           *
           * @description
           *
           * <em>
           * (The remote method definition does not provide any description.)
           * </em>
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * This method returns no data.
           */
          "invoke": {
            url: urlBase + "/Bills/invoke",
            method: "POST"
          },
        }
      );




      /**
       * @ngdoc property
       * @name lbServices.Bill#modelName
       * @propertyOf lbServices.Bill
       * @description
       * The name of the model represented by this $resource,
       * i.e. `Bill`.
       */
      R.modelName = "Bill";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.Push
   * @header lbServices.Push
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `Push` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "Push",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/Push/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.Push#notifyByQuery
           * @methodOf lbServices.Push
           *
           * @description
           *
           * Send a push notification by installation query
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `deviceQuery` – `{object=}` - Installation query
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `Push` object.)
           * </em>
           */
          "notifyByQuery": {
            url: urlBase + "/Push",
            method: "POST"
          },
        }
      );




      /**
       * @ngdoc property
       * @name lbServices.Push#modelName
       * @propertyOf lbServices.Push
       * @description
       * The name of the model represented by this $resource,
       * i.e. `Push`.
       */
      R.modelName = "Push";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.BillCalculator
   * @header lbServices.BillCalculator
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `BillCalculator` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "BillCalculator",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/BillCalculators/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.BillCalculator#getProjectedBill
           * @methodOf lbServices.BillCalculator
           *
           * @description
           *
           * <em>
           * (The remote method definition does not provide any description.)
           * </em>
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `phase` – `{string=}` -
           *
           *  - `conn` – `{string=}` -
           *
           *  - `units` – `{string=}` -
           *
           * @param {Object} postData Request data.
           *
           * This method does not accept any data. Supply an empty object.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `BillCalculator` object.)
           * </em>
           */
          "getProjectedBill": {
            url: urlBase + "/BillCalculators/getProjectedBill",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.BillCalculator#invoke
           * @methodOf lbServices.BillCalculator
           *
           * @description
           *
           * <em>
           * (The remote method definition does not provide any description.)
           * </em>
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * This method returns no data.
           */
          "invoke": {
            url: urlBase + "/BillCalculators/invoke",
            method: "POST"
          },
        }
      );




      /**
       * @ngdoc property
       * @name lbServices.BillCalculator#modelName
       * @propertyOf lbServices.BillCalculator
       * @description
       * The name of the model represented by this $resource,
       * i.e. `BillCalculator`.
       */
      R.modelName = "BillCalculator";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.HourlyPowerUsage
   * @header lbServices.HourlyPowerUsage
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `HourlyPowerUsage` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "HourlyPowerUsage",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/HourlyPowerUsages/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#create
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyPowerUsage` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/HourlyPowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#createMany
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyPowerUsage` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/HourlyPowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#upsert
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyPowerUsage` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/HourlyPowerUsages",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#exists
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/HourlyPowerUsages/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#findById
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyPowerUsage` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/HourlyPowerUsages/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#find
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyPowerUsage` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/HourlyPowerUsages",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#findOne
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyPowerUsage` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/HourlyPowerUsages/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#updateAll
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/HourlyPowerUsages/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#deleteById
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyPowerUsage` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/HourlyPowerUsages/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#count
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/HourlyPowerUsages/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#prototype$updateAttributes
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - PersistedModel id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyPowerUsage` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/HourlyPowerUsages/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyPowerUsage#createChangeStream
           * @methodOf lbServices.HourlyPowerUsage
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/HourlyPowerUsages/change-stream",
            method: "POST"
          },
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.HourlyPowerUsage#updateOrCreate
       * @methodOf lbServices.HourlyPowerUsage
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `HourlyPowerUsage` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.HourlyPowerUsage#update
       * @methodOf lbServices.HourlyPowerUsage
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.HourlyPowerUsage#destroyById
       * @methodOf lbServices.HourlyPowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `HourlyPowerUsage` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.HourlyPowerUsage#removeById
       * @methodOf lbServices.HourlyPowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `HourlyPowerUsage` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];


      /**
       * @ngdoc property
       * @name lbServices.HourlyPowerUsage#modelName
       * @propertyOf lbServices.HourlyPowerUsage
       * @description
       * The name of the model represented by this $resource,
       * i.e. `HourlyPowerUsage`.
       */
      R.modelName = "HourlyPowerUsage";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.DailyPowerUsage
   * @header lbServices.DailyPowerUsage
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `DailyPowerUsage` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "DailyPowerUsage",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/DailyPowerUsages/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#create
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyPowerUsage` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/DailyPowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#createMany
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyPowerUsage` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/DailyPowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#upsert
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyPowerUsage` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/DailyPowerUsages",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#exists
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/DailyPowerUsages/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#findById
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyPowerUsage` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/DailyPowerUsages/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#find
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyPowerUsage` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/DailyPowerUsages",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#findOne
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyPowerUsage` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/DailyPowerUsages/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#updateAll
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/DailyPowerUsages/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#deleteById
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyPowerUsage` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/DailyPowerUsages/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#count
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/DailyPowerUsages/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#prototype$updateAttributes
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - PersistedModel id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyPowerUsage` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/DailyPowerUsages/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyPowerUsage#createChangeStream
           * @methodOf lbServices.DailyPowerUsage
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/DailyPowerUsages/change-stream",
            method: "POST"
          },
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.DailyPowerUsage#updateOrCreate
       * @methodOf lbServices.DailyPowerUsage
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `DailyPowerUsage` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.DailyPowerUsage#update
       * @methodOf lbServices.DailyPowerUsage
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.DailyPowerUsage#destroyById
       * @methodOf lbServices.DailyPowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `DailyPowerUsage` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.DailyPowerUsage#removeById
       * @methodOf lbServices.DailyPowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `DailyPowerUsage` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];


      /**
       * @ngdoc property
       * @name lbServices.DailyPowerUsage#modelName
       * @propertyOf lbServices.DailyPowerUsage
       * @description
       * The name of the model represented by this $resource,
       * i.e. `DailyPowerUsage`.
       */
      R.modelName = "DailyPowerUsage";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.DailyNotification
   * @header lbServices.DailyNotification
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `DailyNotification` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "DailyNotification",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/DailyNotifications/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#create
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyNotification` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/DailyNotifications",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#createMany
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyNotification` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/DailyNotifications",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#upsert
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyNotification` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/DailyNotifications",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#exists
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/DailyNotifications/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#findById
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyNotification` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/DailyNotifications/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#find
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyNotification` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/DailyNotifications",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#findOne
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyNotification` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/DailyNotifications/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#updateAll
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/DailyNotifications/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#deleteById
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyNotification` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/DailyNotifications/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#count
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/DailyNotifications/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#prototype$updateAttributes
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - PersistedModel id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `DailyNotification` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/DailyNotifications/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.DailyNotification#createChangeStream
           * @methodOf lbServices.DailyNotification
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/DailyNotifications/change-stream",
            method: "POST"
          },
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.DailyNotification#updateOrCreate
       * @methodOf lbServices.DailyNotification
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `DailyNotification` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.DailyNotification#update
       * @methodOf lbServices.DailyNotification
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.DailyNotification#destroyById
       * @methodOf lbServices.DailyNotification
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `DailyNotification` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.DailyNotification#removeById
       * @methodOf lbServices.DailyNotification
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `DailyNotification` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];


      /**
       * @ngdoc property
       * @name lbServices.DailyNotification#modelName
       * @propertyOf lbServices.DailyNotification
       * @description
       * The name of the model represented by this $resource,
       * i.e. `DailyNotification`.
       */
      R.modelName = "DailyNotification";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.HourlyNotification
   * @header lbServices.HourlyNotification
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `HourlyNotification` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "HourlyNotification",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/HourlyNotifications/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#create
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyNotification` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/HourlyNotifications",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#createMany
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyNotification` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/HourlyNotifications",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#upsert
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyNotification` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/HourlyNotifications",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#exists
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/HourlyNotifications/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#findById
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyNotification` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/HourlyNotifications/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#find
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyNotification` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/HourlyNotifications",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#findOne
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyNotification` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/HourlyNotifications/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#updateAll
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/HourlyNotifications/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#deleteById
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyNotification` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/HourlyNotifications/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#count
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/HourlyNotifications/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#prototype$updateAttributes
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - PersistedModel id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `HourlyNotification` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/HourlyNotifications/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.HourlyNotification#createChangeStream
           * @methodOf lbServices.HourlyNotification
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/HourlyNotifications/change-stream",
            method: "POST"
          },
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.HourlyNotification#updateOrCreate
       * @methodOf lbServices.HourlyNotification
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `HourlyNotification` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.HourlyNotification#update
       * @methodOf lbServices.HourlyNotification
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.HourlyNotification#destroyById
       * @methodOf lbServices.HourlyNotification
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `HourlyNotification` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.HourlyNotification#removeById
       * @methodOf lbServices.HourlyNotification
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `HourlyNotification` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];


      /**
       * @ngdoc property
       * @name lbServices.HourlyNotification#modelName
       * @propertyOf lbServices.HourlyNotification
       * @description
       * The name of the model represented by this $resource,
       * i.e. `HourlyNotification`.
       */
      R.modelName = "HourlyNotification";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.MonthlyPowerUsage
   * @header lbServices.MonthlyPowerUsage
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `MonthlyPowerUsage` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "MonthlyPowerUsage",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/MonthlyPowerUsages/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#create
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `MonthlyPowerUsage` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/MonthlyPowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#createMany
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `MonthlyPowerUsage` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/MonthlyPowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#upsert
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `MonthlyPowerUsage` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/MonthlyPowerUsages",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#exists
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/MonthlyPowerUsages/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#findById
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `MonthlyPowerUsage` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/MonthlyPowerUsages/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#find
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `MonthlyPowerUsage` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/MonthlyPowerUsages",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#findOne
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `MonthlyPowerUsage` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/MonthlyPowerUsages/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#updateAll
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/MonthlyPowerUsages/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#deleteById
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `MonthlyPowerUsage` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/MonthlyPowerUsages/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#count
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/MonthlyPowerUsages/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#prototype$updateAttributes
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - PersistedModel id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `MonthlyPowerUsage` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/MonthlyPowerUsages/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.MonthlyPowerUsage#createChangeStream
           * @methodOf lbServices.MonthlyPowerUsage
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/MonthlyPowerUsages/change-stream",
            method: "POST"
          },
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.MonthlyPowerUsage#updateOrCreate
       * @methodOf lbServices.MonthlyPowerUsage
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `MonthlyPowerUsage` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.MonthlyPowerUsage#update
       * @methodOf lbServices.MonthlyPowerUsage
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.MonthlyPowerUsage#destroyById
       * @methodOf lbServices.MonthlyPowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `MonthlyPowerUsage` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.MonthlyPowerUsage#removeById
       * @methodOf lbServices.MonthlyPowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `MonthlyPowerUsage` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];


      /**
       * @ngdoc property
       * @name lbServices.MonthlyPowerUsage#modelName
       * @propertyOf lbServices.MonthlyPowerUsage
       * @description
       * The name of the model represented by this $resource,
       * i.e. `MonthlyPowerUsage`.
       */
      R.modelName = "MonthlyPowerUsage";


      return R;
    }]);

  /**
   * @ngdoc object
   * @name lbServices.WeeklyPowerUsage
   * @header lbServices.WeeklyPowerUsage
   * @object
   *
   * @description
   *
   * A $resource object for interacting with the `WeeklyPowerUsage` model.
   *
   * ## Example
   *
   * See
   * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
   * for an example of using this object.
   *
   */
  module.factory(
    "WeeklyPowerUsage",
    ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
      var R = Resource(
        urlBase + "/WeeklyPowerUsages/:id",
        { 'id': '@id' },
        {

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#create
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `WeeklyPowerUsage` object.)
           * </em>
           */
          "create": {
            url: urlBase + "/WeeklyPowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#createMany
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Create a new instance of the model and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `WeeklyPowerUsage` object.)
           * </em>
           */
          "createMany": {
            isArray: true,
            url: urlBase + "/WeeklyPowerUsages",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#upsert
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Update an existing model instance or insert a new one into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `WeeklyPowerUsage` object.)
           * </em>
           */
          "upsert": {
            url: urlBase + "/WeeklyPowerUsages",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#exists
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Check whether a model instance exists in the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `exists` – `{boolean=}` -
           */
          "exists": {
            url: urlBase + "/WeeklyPowerUsages/:id/exists",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#findById
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Find a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           *  - `filter` – `{object=}` - Filter defining fields and include
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `WeeklyPowerUsage` object.)
           * </em>
           */
          "findById": {
            url: urlBase + "/WeeklyPowerUsages/:id",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#find
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Find all instances of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Array.<Object>,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Array.<Object>} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `WeeklyPowerUsage` object.)
           * </em>
           */
          "find": {
            isArray: true,
            url: urlBase + "/WeeklyPowerUsages",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#findOne
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Find first instance of the model matched by filter from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `WeeklyPowerUsage` object.)
           * </em>
           */
          "findOne": {
            url: urlBase + "/WeeklyPowerUsages/findOne",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#updateAll
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Update instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * The number of instances updated
           */
          "updateAll": {
            url: urlBase + "/WeeklyPowerUsages/update",
            method: "POST"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#deleteById
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Delete a model instance by id from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - Model id
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `WeeklyPowerUsage` object.)
           * </em>
           */
          "deleteById": {
            url: urlBase + "/WeeklyPowerUsages/:id",
            method: "DELETE"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#count
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Count instances of the model matched by where from the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `where` – `{object=}` - Criteria to match model instances
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `count` – `{number=}` -
           */
          "count": {
            url: urlBase + "/WeeklyPowerUsages/count",
            method: "GET"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#prototype$updateAttributes
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Update attributes for a model instance and persist it into the data source.
           *
           * @param {Object=} parameters Request parameters.
           *
           *  - `id` – `{*}` - PersistedModel id
           *
           * @param {Object} postData Request data.
           *
           * This method expects a subset of model properties as request parameters.
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * <em>
           * (The remote method definition does not provide any description.
           * This usually means the response is a `WeeklyPowerUsage` object.)
           * </em>
           */
          "prototype$updateAttributes": {
            url: urlBase + "/WeeklyPowerUsages/:id",
            method: "PUT"
          },

          /**
           * @ngdoc method
           * @name lbServices.WeeklyPowerUsage#createChangeStream
           * @methodOf lbServices.WeeklyPowerUsage
           *
           * @description
           *
           * Create a change stream.
           *
           * @param {Object=} parameters Request parameters.
           *
           *   This method does not accept any parameters.
           *   Supply an empty object or omit this argument altogether.
           *
           * @param {Object} postData Request data.
           *
           *  - `options` – `{object=}` -
           *
           * @param {function(Object,Object)=} successCb
           *   Success callback with two arguments: `value`, `responseHeaders`.
           *
           * @param {function(Object)=} errorCb Error callback with one argument:
           *   `httpResponse`.
           *
           * @returns {Object} An empty reference that will be
           *   populated with the actual data once the response is returned
           *   from the server.
           *
           * Data properties:
           *
           *  - `changes` – `{ReadableStream=}` -
           */
          "createChangeStream": {
            url: urlBase + "/WeeklyPowerUsages/change-stream",
            method: "POST"
          },
        }
      );



      /**
       * @ngdoc method
       * @name lbServices.WeeklyPowerUsage#updateOrCreate
       * @methodOf lbServices.WeeklyPowerUsage
       *
       * @description
       *
       * Update an existing model instance or insert a new one into the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *   This method does not accept any parameters.
       *   Supply an empty object or omit this argument altogether.
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `WeeklyPowerUsage` object.)
       * </em>
       */
      R["updateOrCreate"] = R["upsert"];

      /**
       * @ngdoc method
       * @name lbServices.WeeklyPowerUsage#update
       * @methodOf lbServices.WeeklyPowerUsage
       *
       * @description
       *
       * Update instances of the model matched by where from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `where` – `{object=}` - Criteria to match model instances
       *
       * @param {Object} postData Request data.
       *
       * This method expects a subset of model properties as request parameters.
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * The number of instances updated
       */
      R["update"] = R["updateAll"];

      /**
       * @ngdoc method
       * @name lbServices.WeeklyPowerUsage#destroyById
       * @methodOf lbServices.WeeklyPowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `WeeklyPowerUsage` object.)
       * </em>
       */
      R["destroyById"] = R["deleteById"];

      /**
       * @ngdoc method
       * @name lbServices.WeeklyPowerUsage#removeById
       * @methodOf lbServices.WeeklyPowerUsage
       *
       * @description
       *
       * Delete a model instance by id from the data source.
       *
       * @param {Object=} parameters Request parameters.
       *
       *  - `id` – `{*}` - Model id
       *
       * @param {function(Object,Object)=} successCb
       *   Success callback with two arguments: `value`, `responseHeaders`.
       *
       * @param {function(Object)=} errorCb Error callback with one argument:
       *   `httpResponse`.
       *
       * @returns {Object} An empty reference that will be
       *   populated with the actual data once the response is returned
       *   from the server.
       *
       * <em>
       * (The remote method definition does not provide any description.
       * This usually means the response is a `WeeklyPowerUsage` object.)
       * </em>
       */
      R["removeById"] = R["deleteById"];


      /**
       * @ngdoc property
       * @name lbServices.WeeklyPowerUsage#modelName
       * @propertyOf lbServices.WeeklyPowerUsage
       * @description
       * The name of the model represented by this $resource,
       * i.e. `WeeklyPowerUsage`.
       */
      R.modelName = "WeeklyPowerUsage";


      return R;
    }]);


  module
    .factory('LoopBackAuth', function() {
      var props = ['accessTokenId', 'currentUserId'];
      var propsPrefix = '$LoopBack$';

      function LoopBackAuth() {
        var self = this;
        props.forEach(function(name) {
          self[name] = load(name);
        });
        this.rememberMe = undefined;
        this.currentUserData = null;
      }

      LoopBackAuth.prototype.save = function() {
        var self = this;
        var storage = this.rememberMe ? localStorage : sessionStorage;
        props.forEach(function(name) {
          save(storage, name, self[name]);
        });
      };

      LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
        this.accessTokenId = accessTokenId;
        this.currentUserId = userId;
        this.currentUserData = userData;
      }

      LoopBackAuth.prototype.clearUser = function() {
        this.accessTokenId = null;
        this.currentUserId = null;
        this.currentUserData = null;
      }

      LoopBackAuth.prototype.clearStorage = function() {
        props.forEach(function(name) {
          save(sessionStorage, name, null);
          save(localStorage, name, null);
        });
      };

      return new LoopBackAuth();

      // Note: LocalStorage converts the value to string
      // We are using empty string as a marker for null/undefined values.
      function save(storage, name, value) {
        var key = propsPrefix + name;
        if (value == null) value = '';
        storage[key] = value;
      }

      function load(name) {
        var key = propsPrefix + name;
        return localStorage[key] || sessionStorage[key] || null;
      }
    })
    .config(['$httpProvider', function($httpProvider) {
      $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
    }])
    .factory('LoopBackAuthRequestInterceptor', [ '$q', 'LoopBackAuth',
      function($q, LoopBackAuth) {
        return {
          'request': function(config) {

            // filter out non urlBase requests
            if (config.url.substr(0, urlBase.length) !== urlBase) {
              return config;
            }

            if (LoopBackAuth.accessTokenId) {
              config.headers[authHeader] = LoopBackAuth.accessTokenId;
            } else if (config.__isGetCurrentUser__) {
              // Return a stub 401 error for User.getCurrent() when
              // there is no user logged in
              var res = {
                body: { error: { status: 401 } },
                status: 401,
                config: config,
                headers: function() { return undefined; }
              };
              return $q.reject(res);
            }
            return config || $q.when(config);
          }
        }
      }])

  /**
   * @ngdoc object
   * @name lbServices.LoopBackResourceProvider
   * @header lbServices.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
    .provider('LoopBackResource', function LoopBackResourceProvider() {
      /**
       * @ngdoc method
       * @name lbServices.LoopBackResourceProvider#setAuthHeader
       * @methodOf lbServices.LoopBackResourceProvider
       * @param {string} header The header name to use, e.g. `X-Access-Token`
       * @description
       * Configure the REST transport to use a different header for sending
       * the authentication token. It is sent in the `Authorization` header
       * by default.
       */
      this.setAuthHeader = function(header) {
        authHeader = header;
      };

      /**
       * @ngdoc method
       * @name lbServices.LoopBackResourceProvider#setUrlBase
       * @methodOf lbServices.LoopBackResourceProvider
       * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
       * @description
       * Change the URL of the REST API server. By default, the URL provided
       * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
       */
      this.setUrlBase = function(url) {
        urlBase = url;
      };

      /**
       * @ngdoc method
       * @name lbServices.LoopBackResourceProvider#getUrlBase
       * @methodOf lbServices.LoopBackResourceProvider
       * @description
       * Get the URL of the REST API server. The URL provided
       * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
       */
      this.getUrlBase = function() {
        return urlBase;
      };

      this.$get = ['$resource', function($resource) {
        return function(url, params, actions) {
          var resource = $resource(url, params, actions);

          // Angular always calls POST on $save()
          // This hack is based on
          // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
          resource.prototype.$save = function(success, error) {
            // Fortunately, LoopBack provides a convenient `upsert` method
            // that exactly fits our needs.
            var result = resource.upsert.call(this, {}, this, success, error);
            return result.$promise || result;
          };
          return resource;
        };
      }];
    });

})(window, window.angular);
