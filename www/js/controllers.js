angular.module('starter.controllers', [])
  .controller('helpCtrl', function($scope,$state) {

    $scope.clientSideList = [
      { text: "Instructions to use EPAT", value: "ITUE" },
      { text: "Account Setting Info", value: "ASI" },
      { text: "Manage Notifications", value: "MN" },
      { text: "Bills Script Info", value: "BSI" }
    ];
    $scope.backtodashboard = function() {
      $state.go('app.dashboard');
    };


    $scope.data = {
      clientSide: 'ITUE'
    };
    $scope.goSomewhere = function () {
      var path;
      switch($scope.data.clientSide) {
        case 'ITUE': path = 'ITUEStateName'; break;
        case 'ASI': path = 'ASIStateName'; break;
        case 'MN': path = 'MNStateName'; break;
        case 'BSI': path = 'BSIStateName'; break;
      }
      $state.go(path);
    };


  })

  .controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
      $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
      console.log('Doing login', $scope.loginData);

      // Simulate a login delay. Remove this and replace with your login
      // code if using a login system
      $timeout(function() {
        $scope.closeLogin();
      }, 1000);
    };
  })
  .controller('ITUECtrl',function($rootScope,UserEpat,$state,$scope){
    $scope.goback = function() {
      $state.go('app.help');
    };
  })
  .controller('ASICtrl',function($rootScope,UserEpat,$state,$scope){

    $scope.goback = function() {
      $state.go('app.help');
    };
  })
  .controller('BSICtrl',function($rootScope,UserEpat,$state,$scope){

    $scope.goback = function() {
      $state.go('app.help');
    };
  })
  .controller('MNCtrl',function($rootScope,UserEpat,$state,$scope){

    $scope.goback = function() {
      $state.go('app.help');
    };
  })

  .controller('billCtrl', function($scope, $http, $rootScope, UserEpat) {
    function getbill() {
      $http.get($rootScope.baseUrl + 'UserEpats/bill?id=' + localStorage.$LoopBack$currentUserId).then(function(result){
        $scope.bill = result.data;
      });
    }
    function getProjectedBill() {
      $http.get($rootScope.baseUrl + 'UserEpats/projectedBill?id=' + localStorage.$LoopBack$currentUserId).then(function(result){
        $scope.projected = result.data;
      });
    }

    getbill();
    getProjectedBill()
  })
  .controller('settingCtrl', function($rootScope, $scope, UserEpat, $http,$state,$ionicPopup) {
    $scope.User = { }
    $scope.save = function(){
      $http.put($rootScope.baseUrl + 'UserEpats/' + $scope.User.id, {
        firstName: $scope.User.firstName,
        lastName: $scope.User.lastName,
        phaseType:$scope.User.phaseType,
        referenceNo:$scope.User.referenceNo,
        connectionType:$scope.User.connectionType
      });


    };

    function getUserEpat() {
      UserEpat
        .findById({id : localStorage.$LoopBack$currentUserId})
        .$promise
        .then(function (results) {
          $scope.User = results;
        });}
    getUserEpat();
    $scope.backtodashboard = function() {
      $state.go('app.dashboard');
    };
    $scope.confirminfo = function () {
      var msg = $ionicPopup.alert({
        title: 'Saved!',
        template: 'Information you provided have been saved.'
      });
    };
  })

  .controller('notificationsCtrl', function($rootScope, $scope, UserEpat, $http,$interval,$timeout,$ionicPopup,$state) {
    $scope.data = {
      email: false,
      app: false
      //thresh:{}
    };

    $scope.save = function () {
      $http.put($rootScope.baseUrl + 'UserEpats/' + $scope.User.id, {
        notifyEmail: $scope.data.email ? 1 : 0,
        notifyApp: $scope.data.app ? 1 : 0,
        notifyThreshold: $scope.User.notifyThreshold
      });
    };
    function getUserEpat1() {
      UserEpat
        .findById({id : localStorage.$LoopBack$currentUserId})
        .$promise
        .then(function (results) {
          $scope.User = results;
          $scope.data.email = $scope.User.notifyEmail == 1;
          $scope.data.app   = $scope.User.notifyApp == 1;

        });
    }
    getUserEpat1();
    $scope.backtodashboard = function() {
      $state.go('app.dashboard');
    };

    $scope.confirmchoice = function () {
      var msg = $ionicPopup.alert({
        title: 'Saved!',
        template: 'Information recorded.'
      });
    };

  })

  .controller('DashboardCtrl', function ($scope,HourlyPowerUsage, DailyPowerUsage,WeeklyPowerUsage,MonthlyPowerUsage, $rootScope,$timeout,$interval,$ionicLoading,$state ) {
    $scope.hide = function(){

      $ionicLoading.hide();
    };
    var chartData = [];

    $scope.chartType = 'hourly';

    $scope.hourlyPowerUsages = [];
    $scope.dailyPowerUsages = [];
    $scope.weeklyPowerUsages = [];
    $scope.monthlyPowerUsages = [];

    var timezoneOffset = new Date().getTimezoneOffset()*60;

    //chart: {
    //  zoomType: 'x'
    //},

    $scope.chartConfig = {
      options: {
        chart: {
          type: 'line',
          zoomType: 'x'
        }
      },
      title: {
        text: 'Power Consumption Rate'
      },
      subtitle: {
        text: document.ontouchstart === undefined ?
          'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
      },
      xAxis: {
        type: 'datetime'
      },
      yAxis: {
        title: {
          text: 'Watts'
        }
      },
      legend: {
        enabled: false
      },

      plotOptions: {
        area: {
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1
            },
            stops: [
              [0, Highcharts.getOptions().colors[0]],
              [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
            ]
          },
          marker: {
            radius: 2
          },
          lineWidth: 0.1,
          states: {
            hover: {
              lineWidth: 0.1
            }
          },
          threshold: null
        }
      },



      series: [{
        type: 'bar',
//          backgroundColor:blue,
//          color: 'orange',
        name: 'Power Consumption Rate',
        data: chartData
      }],

      loading: false
    };

    function getPowerUsages() {
      if($scope.chartType == 'hourly'){
        HourlyPowerUsage
          .find({filter: {where: {id: localStorage.$LoopBack$currentUserId}, limit: 24, order: 'utc DESC'}})
          .$promise
          .then(function (results) {
            $scope.hourlyPowerUsage = results;
            chartData.splice(0, chartData.length);
            for(var i = 0; i < results.length; i++){
              chartData.push([((results[i].utc - timezoneOffset) * 1000), parseFloat(results[i].watts)]);
            }
          });
      }else if($scope.chartType == 'daily'){
        DailyPowerUsage
          .find({filter: {where: {id: localStorage.$LoopBack$currentUserId}, limit: 7, order: 'utc DESC'}})
          .$promise
          .then(function (results) {
            $scope.dailyPowerUsage = results;
            chartData.splice(0, chartData.length);
            for (var i = 0; i < results.length; i++) {
              chartData.push([((results[i].utc - timezoneOffset) * 1000), parseFloat(results[i].watts)])
            }
          });}
      else if($scope.chartType == 'weekly'){
        WeeklyPowerUsage
          .find({filter: {where: {id: localStorage.$LoopBack$currentUserId}, limit: 4, order: 'utc DESC'}})
          .$promise
          .then(function (results) {
            $scope.weeklyPowerUsage = results;
            chartData.splice(0, chartData.length);
            for (var i = 0; i < results.length; i++) {
              chartData.push([((results[i].utc - timezoneOffset) * 1000), parseFloat(results[i].watts)])
            }
          });}
      else {
        MonthlyPowerUsage
          .find({filter: {where: {id: localStorage.$LoopBack$currentUserId}, limit: 12, order: 'utc DESC'}})
          .$promise
          .then(function (results) {
            $scope.monthlyPowerUsage = results;
            chartData.splice(0, chartData.length);
            for (var i = 0; i < results.length; i++) {
              chartData.push([((results[i].utc - timezoneOffset) * 1000), parseFloat(results[i].watts)])
            }
          });
      }


    }

    getPowerUsages();
    var timer = $interval(getPowerUsages, 60000);
    $scope.doRefresh=getPowerUsages;
    $scope.switchGraph = function (type){
      $scope.chartType = type;
      getPowerUsages();
    };

    // remove timer
    $scope.$on("$destroy", function () {
      $interval.cancel(timer);
      console.log('Canceling timer.');
    });
  })


  .controller('loginCtrl',function($rootScope, $scope,$state,UserEpat, $ionicModal, $ionicPopover,  $location, $ionicPopup,$timeout,$ionicLoading){

    console.log($scope);
    $scope.newUser = { id: ''};
    $scope.login = function() {
      UserEpat
        .login($scope.newUser)
        .$promise
        .then(function(response) {
          $rootScope.notAuthorized = '';
          $rootScope.isAuth = true;
          $rootScope.currentUser = {
            id: response.user.id,
            tokenId: response.id,
            email: response.user.email,
            username: response.user.username
          };
          console.log($rootScope.currentUser);
          $scope.newUser = {id: ''};
          $state.go('app.dashboard');
        }, function(err){
          $scope.notAuthorized = 'username or password  is incorrect';
        });
    }

    $scope.show = function() {
      $ionicLoading.show({
        template: 'Loading...<ion-spinner></ion-spinner>'

      });

    };



    $scope.register = function() {
      $state.go('registration');
    };
    $scope.reset = function() {
      $state.go('forgotPassword');
    };
  })

  .controller('forgotpasswordCtrl',function($scope,$state,UserEpat,$ionicPopup, $timeout){

    $scope.newUser = {id: ''};
    $scope.reset = function() {
      UserEpat
        .resetPassword($scope.newUser)
        .$promise
        .then(function (todo) {
          $scope.newUser = {id: ''};
          $state.go('login');

        });
      $scope.showAlert = function () {
        var alertPopup = $ionicPopup.alert({
          title: 'Email has been sent.',
          template: 'Check your mailing Id to reset your password.'
        });


      }
    };
    $scope.login1 = function() {
      $state.go('login');
    };
    $scope.register1=function() {
      $state.go('registration');

    }
  })

  .controller('LogoutCtrl',function($rootScope,UserEpat,$state){
    UserEpat.logout();
    $rootScope.isAuth = false;
    $rootScope.currentUser = {};
    $state.go('login');
  })




  .controller('registrationCtrl',function($scope,$http, $state, UserEpat,$ionicPopup){

    $scope.login1 = function() {
      $state.go('login');
    };
    $scope.register1=function() {
      $state.go('registration');

    }
    console.log($scope);
    $scope.newUser = { id: ''};

    $scope.register = function() {
      UserEpat
        .create($scope.newUser)
        .$promise
        .then(function(todo) {
          $scope.newUser = {id: ''};
          $state.go('login');
        });
      $scope.confirmreg = function () {
        var msg = $ionicPopup.alert({
          title: 'Thank-you for Registering into EPAT',
          template: 'Confirm your registration by clicking on the link sent to your mailing id.'
        });
        var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
        $scope.analyze = function (value) {
          if (strongRegex.test(value)) {
            $scope.passwordStrength["background-color"] = "green";

          } else if (mediumRegex.test(value)) {
            $scope.passwordStrength["background-color"] = "orange";
          } else {
            $scope.passwordStrength["background-color"] = "red";
          }
        };
        $scope.passwordStrength = {
          "float": "left",
          "width": "100px",
          "height": "25px",
          "margin-left": "5px"

        };
      }
    }
  });








